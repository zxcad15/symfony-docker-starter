# Symfony-Docker-Starterkit

This project is a simple starter kit to use Symfony with PostgreSQL through Docker and Docker Compose.

## Getting Started

These instructions will get you a copy of the project of Symfony 4.3 using a PostgreSQL database, running on your local machine for development purposes through Docker.

### Prerequisites

* [Docker](https://docs.docker.com/install/)

* [Docker Compose](https://docs.docker.com/compose/install/)

* Any IDE (like [Visual Studio Code](https://code.visualstudio.com/))

### Deployment






